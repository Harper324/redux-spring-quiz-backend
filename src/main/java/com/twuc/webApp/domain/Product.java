package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 32, nullable = false)
    private String name;

    @Column(nullable = false)
    private int price;

    @Column(length = 16, nullable = false)
    private String unit;

    @Column(length = 64,nullable = false)
    private String image;

    public Product() {
    }

    public Product(String name, int price, String unit, String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
