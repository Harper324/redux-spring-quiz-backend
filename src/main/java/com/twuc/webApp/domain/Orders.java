package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class Orders {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false)
    private String unit;

    @Column(nullable = false)
    private Integer count;


    public Orders(String name, Integer price, String unit, Integer count) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
    }

    public Orders(String name) {
        this.name = name;
    }

    public Orders() {
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
