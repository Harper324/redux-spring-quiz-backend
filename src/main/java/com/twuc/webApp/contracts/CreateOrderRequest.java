package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;

public class CreateOrderRequest {

    private Long id;
    @NotNull
    private String name;
    @NotNull
    private Integer price;
    @NotNull
    private String unit;
    @NotNull
    private Integer count;

    public CreateOrderRequest(@NotNull String name, @NotNull Integer price, @NotNull String unit, @NotNull Integer count) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
    }

    public CreateOrderRequest(Long id, @NotNull String name, @NotNull Integer price, @NotNull String unit, @NotNull Integer count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public CreateOrderRequest() {
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public Integer getCount() {
        return count;
    }
}

