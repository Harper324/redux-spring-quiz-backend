package com.twuc.webApp.contracts;

public class GetOrderResponse {

    private long id;
    private String name;
    private int price;
    private String unit;
    private int count;

    public GetOrderResponse(long id, String name, int price, String unit, int count) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.count = count;
    }

    public GetOrderResponse() {
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public int getCount() {
        return count;
    }
}
