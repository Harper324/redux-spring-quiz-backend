package com.twuc.webApp.contracts;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateProductRequest {
    @NotNull
    @Size(min = 1, max = 32)
    private String name;
    @NotNull
    private int price;
    @NotNull
    @Size(min = 1, max = 16)
    private String unit;
    @NotNull
    private String image;

    public CreateProductRequest() {
    }

    public CreateProductRequest(@NotNull @Size(min = 1, max = 32) String name, @NotNull int price, @NotNull @Size(min = 1, max = 16) String unit, @NotNull String image) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImage() {
        return image;
    }
}
