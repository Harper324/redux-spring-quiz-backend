package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreateProductRequest;
import com.twuc.webApp.contracts.GetProductResponse;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/products")
@CrossOrigin(origins = "*")
public class ProductController {
    private final ProductRepository productRepository;

    public ProductController(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @GetMapping
    public ResponseEntity<List<GetProductResponse>> getAll() {
        List<GetProductResponse> products = productRepository.findAll(
            Sort.by(Sort.Direction.ASC, "id")).stream()
            .map(p -> new GetProductResponse(p.getId(),p.getName(),p.getPrice(),p.getUnit(),p.getImage()))
            .collect(Collectors.toList());
        return ResponseEntity.ok(products);
    }

    @PostMapping
    public ResponseEntity postProduct(@RequestBody @Valid CreateProductRequest request) {
        Product product=new Product(request.getName(),request.getPrice(),request.getUnit(),request.getImage());
        productRepository.saveAndFlush(product);
        return ResponseEntity.status(HttpStatus.CREATED).body(product);
    }
}
