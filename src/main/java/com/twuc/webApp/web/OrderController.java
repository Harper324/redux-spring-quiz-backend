package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreateOrderRequest;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.domain.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

//    @Autowired
//    private ProductRepository productRepository;

    @PostMapping
    public ResponseEntity postOrder(@RequestBody @Valid CreateOrderRequest request) {
        Orders order = new Orders(request.getName(),request.getPrice(),request.getUnit(),request.getCount());
        orderRepository.save(order);
        return ResponseEntity.status(HttpStatus.CREATED).body(order);
    }

    @PutMapping
    public ResponseEntity updateOrder(@RequestBody @Valid CreateOrderRequest request) {
        int count=request.getCount();
        Long id=request.getId();
        Orders order=orderRepository.findById(id).orElseThrow(RuntimeException::new);
        order.setCount(count);

        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteOrder(@PathVariable Long id) {
        orderRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body(id);
    }
}
