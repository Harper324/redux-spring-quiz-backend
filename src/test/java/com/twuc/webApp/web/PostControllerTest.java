package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateOrderRequest;
import com.twuc.webApp.domain.Orders;
import com.twuc.webApp.domain.OrderRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class PostControllerTest extends ApiTestBase {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    private List<Long> createProducts(List<Product> products) {
        List<Long> ids = new ArrayList<>();

        clear(em -> {
            productRepository.saveAll(products);
            productRepository.flush();
            ids.addAll(products.stream().map(Product::getId).collect(Collectors.toList()));
        });

        return ids;
    }

    @Test
    void should_get_all_products() throws Exception {
        createProducts(Arrays.asList(new Product("product1", 1, "unit1","url1")
                , new Product("product2", 2, "unit2","url2")));
        String json = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andReturn().getResponse().getContentAsString();

        PostResponseForTest[] products = new ObjectMapper().readValue(json, PostResponseForTest[].class);

        assertEquals(2, products.length);
        assertEquals(1, products[0].getId());
        assertEquals("product1", products[0].getName());
        assertEquals(1, products[0].getPrice());
        assertEquals("unit1", products[0].getUnit());
        assertEquals(2, products[1].getId());
        assertEquals("product2", products[1].getName());
        assertEquals(2, products[1].getPrice());
        assertEquals("unit2", products[1].getUnit());
    }

    @Test
    void should_post_order_and_return_201() throws Exception {

        Orders order = new Orders("product1", 1, "unit1", 1);
        getMockMvc().perform(post("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(order)))
                .andExpect(status().is(201));

    }

    @Test
    void should_post_product_and_return_201() throws Exception {

        Product product = new Product("product1", 1, "unit1","url1");

        getMockMvc().perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(product)
                ))
                .andExpect(status().is(201));
    }

    @Test
    void should_delete_order_and_return_200() throws Exception {

        Orders orders = new Orders("product1", 2, "unit1", 2);
        Long id = orderRepository.saveAndFlush(orders).getId();
        getMockMvc().perform(delete(String.format("/api/orders/%s", id))).andExpect(status().is(200));
    }

    @Test
    void should_update_order_and_return_200() throws Exception {

        CreateOrderRequest request = new CreateOrderRequest(1L, "product1", 2, "unit1", 2);
        Orders orderFirst = new Orders("product1", 2, "unit1", 1);
        orderRepository.saveAndFlush(orderFirst);

        getMockMvc().perform(put("/api/orders")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                        new ObjectMapper()
                                .writeValueAsString(request)
                ))
                .andExpect(status().is(200));
    }
}

class PostResponseForTest {
    private long id;
    private String name;
    private int price;
    private String unit;
    private String image;

    public String getImage() {
        return image;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }
}

